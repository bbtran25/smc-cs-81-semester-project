const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const PATHS = {
  app: path.resolve(__dirname, "./src"),
  dist: path.resolve(__dirname, "./dist"),
};

const commonConfig = {
  context: process.cwd(),
  entry: {
    app: ["@babel/polyfill", "webpack-dev-server/client?http://localhost:8080/", PATHS.app],
    vendor: ["lodash", "prop-types", "react", "react-dom", "redux", "react-router", "react-router-dom", "react-redux"],
  },
  output: {
    filename: "[name].js",
    path: PATHS.dist,
    publicPath: "/",
    sourceMapFilename: "[name].js.map",
  },
  module: {
    rules: [
      {
        test: /.*\.jsx?$/,
        include: PATHS.app,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
            },
          },
        ],
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Benjamin Tran Semester Project",
      template: `${PATHS.app}/index.html`,
      filename: "./index.html",
    }),
  ],
  resolve: {
    extensions: [".webpack.js", ".web.js", ".mjs", ".js", ".jsx", ".json"],
  },
  devServer: {
    historyApiFallback: true,
    publicPath: "/",
    contentBase: "./dist",
    compress: true,
    port: 8080,
    proxy: {
      "/api/*": {
        target: {
          host: "localhost",
          protocol: "http:",
          port: 3030,
        },
      },
    },
  },
  externals: {
    "react/addons": true,
    "react/lib/ExecutionEnvironment": true,
    "react/lib/ReactContext": true,
  },
};

module.exports = commonConfig;
