# SMC CS 81 Semester Project
## Version 1.0.0

Final Project - A yelp replica MVP with a random restaurant generator for the indecisive.

# Tech Stack

## Frontend
* [React](https://facebook.github.io/react/)-[Redux](https://github.com/reactjs/redux) - JS libraries for rendering page views and maintaining state
* [React Router v4](https://github.com/ReactTraining/react-router) - Routing library for React applications, enabling URL for specific views
* [Material-UI](https://material-ui.com/) - mobile first styling framework
* [Styled Components](https://www.styled-components.com/) - CSS

## Backend
* [Node.js](https://nodejs.org/en/) with [Express](http://expressjs.com/) for serving pages
  
## Dev/Build
* [Webpack](https://webpack.github.io/) for bundling/scaffolding and [Babel](https://babeljs.io/) for transpiling

## APIs
* [Yelp Fusion](https://www.yelp.com/developers/documentation/v3/) 


## Install
#### Global Installs

For running Node server
```
$ brew install node
```

For building and deploying: Webpack
```
$ yarn global add webpack-cli
```

#### Local Install
Download all dependencies listed under package.json
```
$ yarn install
```

#### To Run the Application
Initiate both Development and Production servers by running the following:

```$ yarn dev``` and in a separate terminal run ```$ yarn build``` and then ```$ yarn start```

Frontend development changes are reflected immediately on ```http://localhost:8080```
Production server is on ```http://localhost:3030```

## License
MIT

## Project Experience/Reflection
This project has been really fun to work on. I decided to go with the states technologies for this because I have not been coding much in React/Redux lately. This made it a perfect opportunity for me to relearn and practice javascript and some of its frameworks. I particularly enjoyed working on the express server because I am hoping to transition to become a backend developer. The biggest pain point for this project was underestimating the amount of time that it would take. There are so many issue that you can run into, on both the client and the server. I would have given myself more time to complete a more polished application. 
