const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const webpack = require("webpack");
const qs = require("query-string");
require("dotenv").config();
const axios = require("axios");

const app = express();

const PORT = process.env.PORT || 3030;

app.set("port", PORT);

// ADD ACCESS CONTROL HEADERS MIDDLEWARE TO ALL REQUESTS
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const baseURL = "https://api.yelp.com/v3";
const get = (resource, params) => {
  console.log("params", params);
  const options = params ? `?${qs.stringify(params)}` : "";
  const url = `${baseURL}/${resource}${options}`;

  console.log("URL", url);
  // console.log("API KEY:", process.env.YELP_API_KEY);

  return axios
    .request({
      url,
      method: "get",
      headers: {
        Authorization: `Bearer ${process.env.YELP_API_KEY}`,
      },
    })
    .then(response => {
      console.log("SUCCESS");

      return response.data;
    })
    .catch(err => {
      console.error("ERROR:", err);
      throw err;
    });
};

app.get("/api", (req, res) => {
  res.send("Hello world!");
});

app.get("/api/health", (req, res) => res.send("OK"));

// GET https://api.yelp.com/v3/businesses/search
app.post("/api/search", async (req, res) => {
  res.set("Content-Type", "application/json");
  const data = req.body;

  try {
    const result = await get("businesses/search", data);

    res.status(200);
    res.send(JSON.stringify(result));
  } catch (err) {
    console.log("ERROR:", err);
    res.status(400);
    res.send(JSON.stringify(err));
  }
});

// GET https://api.yelp.com/v3/businesses/{id}
app.post("/api/business", async (req, res) => {
  res.set("Content-Type", "application/json");
  const data = req.body;
  const { id } = data;

  try {
    const result = await get(`businesses/${id}`, null);

    res.status(200);
    res.send(JSON.stringify(result));
  } catch (err) {
    console.log("ERROR:", err);
    res.status(400);
    res.send(JSON.stringify(err));
  }
});

// GET https://api.yelp.com/v3/businesses/{id}/reviews
app.post("/api/business-reviews", async (req, res) => {
  res.set("Content-Type", "application/json");
  const data = req.body;
  const { id } = data;

  try {
    const result = await get(`businesses/${id}/reviews`, null);

    res.status(200);
    res.send(JSON.stringify(result));
  } catch (err) {
    console.log("ERROR:", err);
    res.status(400);
    res.send(JSON.stringify(err));
  }
});

if (process.env.NODE_ENV === "production") {
  console.log("IN PRODUCTION");
  app.use(express.static(path.join(__dirname, "./dist")));
  app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "dist/index.html"));
  });
} else {
  /* eslint-disable-next-line */
  const webpackMiddleware = require("webpack-dev-middleware");
  /* eslint-disable-next-line */
  // const webpackHotMiddleware = require("webpack-hot-middleware");
  /* eslint-disable-next-line */
  const webpackConfig = require("./webpack.config.dev.js")();

  app.use(
    webpackMiddleware(webpack(webpackConfig), {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath,
    }),
  );
  // app.use(webpackHotMiddleware(webpack(webpackConfig)));
}

app.listen(PORT, () => console.log(`Listening on ${PORT} ...`));
