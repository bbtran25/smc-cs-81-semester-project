import { createGlobalStyle } from "styled-components";
import styledNormalize from "styled-normalize";
import { COLORS } from "../constants";

export default () => createGlobalStyle`
  ${styledNormalize};

  html {
    height: 100%;
    width: 100%;
  }

  body {
    height: 100%;
    font-family: "Lato", "Open Sans", sans-serif;
    font-weight: 300;
    font-size: 14px;
    color: ${COLORS.text_default};

    #main {
      height: 100%;
      width: 100%;
    }
  }

  @media (max-width: 1300px) {
    body {
      font-size: 13px;
    }
  }

  @media (max-width: 1100px) {
    body {
      font-size: 12px;
    }
  }
  @media (max-width: 900px) {
    body {
      font-size: 10px;
    }
  }

  .text-0 {
    font-size: 13px;
    letter-spacing: 0.03rem;
  }
  .text-1 {
    font-size: 16px;
  }
  .text-2 {
    font-size: 20px;
  }
  .text-3 {
    font-size: 24px;
  }
  .text-4 {
    font-size: 30px;
  }

  a {
    color: inherit;
    text-decoration: none;
  }
  
  .strong {
    font-weight: 600;
  }


  .rounded {
    border-radius: 5px;
  }

  .circle {
    border-radius: 50%;
  }
`;
