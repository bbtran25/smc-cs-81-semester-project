import styled from "styled-components";
// import { COLORS } from "../constants";

export const ContentContainer = styled.div`
  padding: 0 4em 1em 1em;
  width: 100%;
  box-sizing: border-box;
`;

export const FlexContainer = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .content {
    box-sizing: border-box;
    width: 100%;
    padding: 10px 0 2px 0;
  }
`;

export const Fixed = styled.div`
  width: 100%;
  flex-shrink: 0;
`;

export const Scroll = styled.div`
  width: 100%;
  overflow-y: scroll;
  flex-shrink: 1;
  flex-grow: 0;

  &::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
  }
`;
