import React from "react";
import { connect } from "react-redux";
import Home from "./home";
import { fetchBusinesses, selectors as BUSINESSES_SELECTORS } from "../../redux/businesses";

export const HomeContainer = ({ ...props }) => {
  console.log("HOME PROPS!", props);

  return <Home {...props} />;
};

export function mapStateToProps(state) {
  console.log("STATE", state);
  const isRequesting = BUSINESSES_SELECTORS.getIsRequesting(state);
  if (isRequesting) return { isRequesting };

  const error = BUSINESSES_SELECTORS.getError(state);
  if (error) return { error };

  const businesses = BUSINESSES_SELECTORS.getData(state);

  return {
    businesses,
    isRequesting,
    error,
  };
}
export default connect(mapStateToProps, { fetchBusinesses })(HomeContainer);
