import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import NearMeIcon from "@material-ui/icons/NearMe";
import SearchIcon from "@material-ui/icons/Search";
import styled from "styled-components";
import NavBar from "../../components/navbar";
import BusinessListItem from "../../components/busines-item";
import { FlexContainer } from "../../styles/shared-styles";
import Loader from "../../components/loader";
import { COLORS } from "../../constants";

export class HomePage extends PureComponent {
  constructor() {
    super();
    this.state = {
      searchTerm: "",
      location: "",
      usingCoords: false,
      longitude: "",
      latitude: "",
      showOne: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleOnFocus = this.handleOnFocus.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.handleLuckySearchSubmit = this.handleLuckySearchSubmit.bind(this);
  }

  handleGetGeoLocation() {
    console.log("CLICKEd");
    const geo = window.navigator && window.navigator.geolocation;
    if (geo) {
      geo.getCurrentPosition(
        position => {
          console.log("POSITION:", position);
          const geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          const ll = `${geolocation.lat},${geolocation.lng}`;

          if (ll) {
            this.setState({
              longitude: geolocation.lng,
              latitude: geolocation.lat,
              usingCoords: true,
              location: "Current Location",
            });
          }
        },
        error => {
          console.error("error:", error);
        },
      );
    }
  }

  handleOnFocus() {
    const { location } = this.state;
    if (location === "Current Location") {
      this.setState({ location: "" });
    }
  }

  handleChange(e, field) {
    const { value } = e.target;
    this.setState({
      usingCoords: false,
      [field]: value,
    });
  }

  handleSearchSubmit() {
    this.setState({
      showOne: false,
    });
    const { fetchBusinesses } = this.props;
    const { location, searchTerm, longitude, latitude } = this.state;
    let params = {};
    if (longitude && latitude) {
      params = { longitude, latitude, term: searchTerm, limit: "50", sort_by: "review_count", radius: 16093 };
    } else {
      params = { location, term: searchTerm, limit: "50", sort_by: "review_count", radius: 16093 };
    }

    console.log("PARAMS:", params);
    fetchBusinesses(params);
  }

  handleLuckySearchSubmit() {
    this.setState({
      showOne: true,
    });
    const { fetchBusinesses } = this.props;
    const { location, searchTerm, longitude, latitude } = this.state;
    let params = {};
    if (longitude && latitude) {
      params = { longitude, latitude, term: searchTerm, limit: "50", sort_by: "review_count", radius: 16093 };
    } else {
      params = { location, term: searchTerm, limit: "50", sort_by: "review_count", radius: 16093 };
    }

    console.log("PARAMS:", params);
    fetchBusinesses(params);
  }

  render() {
    const { searchTerm, location, usingCoords, showOne } = this.state;
    const { isRequesting, businesses } = this.props;
    this.singleBusiness =
      showOne && businesses && businesses.businesses[Math.round(Math.random() * (businesses.businesses.length - 1))];

    return (
      <Container>
        <NavBar />
        <Style>
          <div className="home">
            <div className="search-wrapper">
              <TextField
                id="outlined-search"
                label="What Are You Craving?"
                type="search"
                className="search-input"
                margin="normal"
                variant="outlined"
                value={searchTerm}
                onChange={e => this.handleChange(e, "searchTerm")}
              />
              <TextField
                id="outlined-search"
                label="Location (City of Zipcode)"
                type="search"
                className={`location-zip-input ${classnames({
                  "bold-blue": usingCoords,
                })}`}
                margin="normal"
                variant="outlined"
                value={location}
                onFocus={this.handleOnFocus}
                onChange={e => this.handleChange(e, "location")}
              />
              <IconButton
                variant="contained"
                color="primary"
                className="new-me-icon"
                onClick={this.handleGetGeoLocation.bind(this)}
              >
                <NearMeIcon />
              </IconButton>
              <IconButton variant="contained" color="primary" className="search-icon" onClick={this.handleSearchSubmit}>
                <SearchIcon />
              </IconButton>
              <Button
                variant="outlined"
                color="primary"
                className="search-text-btn"
                onClick={this.handleLuckySearchSubmit}
              >
                Im Feeling Lucky!
              </Button>
            </div>
            {!isRequesting ? (
              businesses &&
              (showOne ? (
                <BusinessListItem {...this.singleBusiness} />
              ) : (
                businesses.businesses.map(item => {
                  return <BusinessListItem key={item.id} {...item} />;
                })
              ))
            ) : (
              <Loader />
            )}
          </div>
        </Style>
      </Container>
    );
  }
}

const Style = styled(FlexContainer)`
  .location-zip-input,
  .search-input {
    width: 400px;
    margin: 10px;
  }

  .search-wrapper {
    margin: 20px 0; 
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .bold-blue {
    input {
      color: ${COLORS.app_blue};
    }
    font-weight: 700;
  }
`;

HomePage.propTypes = {
  fetchBusinesses: PropTypes.func,
  isRequesting: PropTypes.bool,
  businesses: PropTypes.object,
};

export default HomePage;
