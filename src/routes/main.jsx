import React from "react";
import { Switch, Route } from "react-router-dom";
import Landing from "./landing-page";
import Home from "./home";

export const MainRoutes = () => {
  console.log("RENDERING ROUTER");

  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/blah" component={Landing} />
    </Switch>
  );
};

export default MainRoutes;
