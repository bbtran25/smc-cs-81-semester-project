import React from "react";
import { ConnectedRouter } from "connected-react-router";
import { createBrowserHistory } from "history";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { render } from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./redux/store";
import MainRoutes from "./routes/main";
import injectGlobalStyles from "./styles/global-styles";
import { COLORS } from "./constants";

const GlobalStyles = injectGlobalStyles();

const theme = createMuiTheme({
  palette: {
    primary: {
      main: COLORS.app_red,
    },
    secondary: {
      main: COLORS.app_blue,
    },
  },
  typography: {
    fontFamily: "Lato",
  },
});

export const history = createBrowserHistory();
export const store = configureStore(history);

console.log("LOADED");

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <MainRoutes />
      </ThemeProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("main"),
);
