export const COLORS = {
  gray_0: "#F9F9F9",
  gray_1: "#F7F7F7",
  gray_2: "#D8D8D8",
  gray_3: "#B1B1B1",
  gray_4: "#9C9C9C",
  gray_5: "#707070",
  text_default: "#333333",
  app_red: "#d32323",
  app_blue: "#0073bb",
};

export const SIDEBAR_WIDTH = {
  desktop: "240px",
};

export const GLOBAL_EASE = "cubic-bezier(0.165, 0.84, 0.44, 1)";
