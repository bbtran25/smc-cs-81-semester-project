import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import business from "./business";
import businesses from "./businesses";

export const appReducer = history => {
  return combineReducers({
    router: connectRouter(history),
    business,
    businesses,
  });
};

export default appReducer;
