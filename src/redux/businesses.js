import axios from "axios";

export const actions = {
  REQUEST: "BUSINESSES/REQUEST",
  DATA_SUCCESS: "BUSINESSES/DATA/SUCCESS",
  DATA_FAILURE: "BUSINESSES/DATA/FAILURE",
};

export const initialState = {
  isRequesting: false,
  error: null,
  data: null,
};

export function fetchBusinesses(params) {
  const url = `http://localhost:3030/api/search`;
  console.log(params);
  const request = axios.request({
    url,
    method: "post",
    data: params,
  });

  return dispatch => {
    dispatch({ type: actions.REQUEST, payload: null });
    request
      .then(({ data }) => {
        dispatch({ type: actions.DATA_SUCCESS, payload: data });
      })
      .catch(error => {
        dispatch({ type: actions.DATA_FAILURE, payload: error });
      });
  };
}

export function businessesReducer(state = initialState, action) {
  switch (action.type) {
    case actions.REQUEST:
      return { ...state, isRequesting: true };
    case actions.DATA_SUCCESS:
      return { ...state, data: action.payload, isRequesting: false };
    case actions.DATA_FAILURE:
      return { ...state, error: action.payload, isRequesting: false };
    default:
      return state;
  }
}

export const selectors = {
  getIsRequesting: state => state.businesses.isRequesting,
  getError: state => state.businesses.error,
  getData: state => state.businesses.data,
};

export default businessesReducer;
