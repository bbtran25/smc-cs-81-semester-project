import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";

import rootReducer from "../reducer";

export default function configureStore(history) {
  const middleware = routerMiddleware(history);
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(rootReducer(history), composeEnhancers(applyMiddleware(middleware, thunk)));

  return store;
}
