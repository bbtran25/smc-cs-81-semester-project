import axios from "axios";

export const actions = {
  REQUEST: "BUSINESS/REQUEST",
  DATA_SUCCESS: "BUSINESS/DATA/SUCCESS",
  DATA_FAILURE: "BUSINESS/DATA/FAILURE",
};

export const initialState = {
  isRequesting: true,
  error: null,
  data: null,
};

export function fetchBusiness(params) {
  const url = `http://localhost:30030/api/business`;
  const request = axios.post(url, {
    data: params,
  });

  return dispatch => {
    dispatch({ type: actions.REQUEST, payload: null });
    request
      .then(({ data }) => {
        dispatch({ type: actions.DATA_SUCCESS, payload: data });
      })
      .catch(error => {
        dispatch({ type: actions.DATA_FAILURE, payload: error });
      });
  };
}

export function businessReducer(state = initialState, action) {
  switch (action.type) {
    case actions.REQUEST:
      return state;
    case actions.DATA_SUCCESS:
      return { ...initialState, data: action.payload };
    case actions.DATA_FAILURE:
      return { ...initialState, error: action.payload, isRequesting: false };
    default:
      return state;
  }
}

export default businessReducer;
