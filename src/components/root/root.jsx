import React from "react";
import { Provider } from "react-redux";
import PropTypes from "prop-types";
import { ConnectedRouter } from "connected-react-router";

import MainRoutes from "../../routes/main";

export const Root = props => {
  const { store, history } = props;

  return (
    <Provider store={store}>
      <div className="root">
        <ConnectedRouter history={history}>
          <MainRoutes />
        </ConnectedRouter>
      </div>
    </Provider>
  );
};

Root.propTypes = {
  store: PropTypes.object,
  history: PropTypes.object,
};

export default Root;
