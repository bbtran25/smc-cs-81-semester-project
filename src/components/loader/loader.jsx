import React from "react";
import styled from "styled-components";
import { CubeGrid } from "better-react-spinkit";
import { COLORS } from "../../constants";

export const BusinessListItem = () => {
  return (
    <Style className="loader">
      <CubeGrid size={50} color={COLORS.app_red} />
    </Style>
  );
};

const Style = styled.div`
  width: 100%;
  height: 50vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default BusinessListItem;
