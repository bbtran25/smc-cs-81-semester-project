import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

export const NavBar = props => {
  const { title } = props;

  return (
    <Style className="navbar-wrapper">
      <AppBar className="navbar" position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className="title-text">
            {title || "Feed Me Something Good!"}
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Style>
  );
};

const Style = styled.div`
  .title-text {
    flex-grow: 1;
  }
`;

NavBar.propTypes = {
  title: PropTypes.string,
};

export default NavBar;
