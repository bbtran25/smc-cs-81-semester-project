import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Paper from "@material-ui/core/Paper";

export const BusinessListItem = props => {
  const { name, display_phone, image_url, price, location, url } = props;
  const { display_address } = location;

  return (
    <Style className="business-list-item">
      <a href={url} target="_blank" rel="noopener noreferrer">
        <Paper>
          <div className="business-info">
            <div className="img-wrapper">
              <img className="business-img" src={image_url} alt="business" />
            </div>
            <div className="business-data">
              <h2>{name}</h2>
              <p>
                <span>Price Range: {price}</span>
              </p>
              <p>
                <span>
                  {display_address[0]}, {display_address[1]}
                </span>
              </p>
              <p>
                <span>Phone: {display_phone}</span>
              </p>
            </div>
          </div>
        </Paper>
      </a>
    </Style>
  );
};

const Style = styled.div`
  margin: 10px;
  .business-info {
    display: flex;
    flex-direction: row;
  }
  .img-wrapper {
    margin: 10px;
    .business-img {
      width: 150px;
      height: 150px;
      object-fit: contain;
    }
  }
`;

BusinessListItem.propTypes = {
  name: PropTypes.string,
  location: PropTypes.object,
  display_phone: PropTypes.string,
  image_url: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
};
export default BusinessListItem;
